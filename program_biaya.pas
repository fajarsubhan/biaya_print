(*
 * @Title       : Biaya Print UAP
 * @Matkul		: Algoritma
 * @CreatedBy	: Fajar Subhan
 * @Nim			: 202043500578
 * @License 	: The GNU General Public License (GNU GPL or simply GPL) https://www.gnu.org/
 * @Docs		: https://www.freepascal.org/docs.html
*)


program print_data;
uses crt; // crt.tpu library import

{ ======== Deklrasi Variable ========= }
var 
  Jl,total_bayar : real;
var 
  warna : string;
  
 { ========= Start Main Program =========== }
begin 
clrscr;

write('Berapa lembar yang akan di print : ');
readln(Jl);

write('Masukan warna : ');
readln(warna);

if(Jl > 20) then 
begin 
  if(warna <> 'Hitam Putih') then 
    begin 
	  total_bayar := Jl * 2000;
	end 
  else 
    begin 
	  total_bayar := Jl * 1500;
	end;
end 
else 
  begin 
    total_bayar := Jl * 2500;
  end;
  
  write('Yang harus dibayar sebanyak : ', total_bayar:2:0);
end.
 { ========= End Main Program =========== }